from django.db import models

from properties.models import HardSkill
from users.models import User


class Vacancy(models.Model):
    LEVEL_OPTIONS = [
        ('Junior', 'Junior'),
        ('Middle', 'Middle'),
        ('Senior', 'Senior')
    ]
    OCCUPATION_OPTIONS = [
        ('10 hours per week', '10 hours per week'),
        ('20 hours per week', '20 hours per week'),
        ('40 hours per week', '40 hours per week')
    ]
    company = models.ForeignKey(
        User,
        verbose_name='Компания',
        on_delete=models.CASCADE,
    )
    # soft_skill = models.ForeignKey(
    #     'SoftSkill',
    #     on_delete=models.SET_NULL,
    #     verbose_name='Гибкий навык',
    #     null=True,
    #     blank=True
    # )
    hard_skill = models.ManyToManyField(
        HardSkill,
        verbose_name='Проф навык',
    )
    start_date = models.DateField(
        verbose_name='Дата начала',
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True
    )
    occupation = models.CharField(
        verbose_name='Занятость',
        choices=OCCUPATION_OPTIONS,
        max_length=20,
        default='40 hours per week'
    )
    level = models.CharField(
        verbose_name='Уровень',
        choices=LEVEL_OPTIONS,
        max_length=15
    )
    description = models.CharField(
        max_length=500,
        verbose_name='Описание',
        null=True,
        blank=True
    )
    file = models.FileField(
        verbose_name='Иконка',
        upload_to='vacancy/',
        null=True,
        blank=True
    )
    created_at = models.DateField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )
    is_approved = models.BooleanField(
        default=False,
        verbose_name='Активный аккаунт',
    )

    class Meta:
        verbose_name = 'Вакансия'
        verbose_name_plural = 'Вакансии'

    def __str__(self):
        return f'{self.company} {self.level} {self.occupation} ({self.start_date})'


class Subscription(models.Model):
    vacancy = models.ForeignKey(
        Vacancy,
        verbose_name='Пользователь',
        on_delete=models.SET_NULL,
        null=True,
    )
    status = models.CharField(
        verbose_name='Статус',
        max_length=30,
        default='unpaid',
        blank=True
    )
    subscription_id = models.CharField(
        verbose_name='subscription_id для Stripe',
        max_length=100,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписки'

    def __str__(self):
        return f'{self.subscription_id}'
