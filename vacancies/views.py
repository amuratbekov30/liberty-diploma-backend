from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from django.db.models import Avg, Subquery, OuterRef

from candidates.models import Candidate, CandidateHard
from candidates.serializers import CandidateSerializer
from vacancies.models import Vacancy
from vacancies.serializers import VacancySerializer


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def vacancies_list(request):
    if request.method == 'GET':
        vacancies = Vacancy.objects.filter(company=request.user).order_by('-created_at')
        serializer = VacancySerializer(vacancies, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET', 'DELETE'])
@permission_classes((IsAuthenticated,))
def vacancy_detail(request, pk):
    try:
        vacancy = Vacancy.objects.prefetch_related('hard_skill').get(id=pk)
    except Vacancy.DoesNotExist:
        return Response({'message': 'Application not found'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = VacancySerializer(vacancy)
        if vacancy.level == "Junior":
            minimum = 0
            maximum = 2.5
        elif vacancy.level == "Middle":
            minimum = 2.5
            maximum = 4
        elif vacancy.level == "Senior":
            minimum = 4
            maximum = 5.1
        else:
            minimum = 0
            maximum = 5.1
        hard_skills = vacancy.hard_skill.all().values_list('name', flat=True)
        candidates = Candidate.objects.all().annotate(
            need_skills_avg=Subquery(
                CandidateHard.objects.filter(
                    candidate_id=OuterRef('id'),
                    hard_skill__name__in=hard_skills
                ).values('candidate_id').annotate(a=Avg('score')).values('a')
            ),
        ).filter(need_skills_avg__gte=minimum, need_skills_avg__lt=maximum)
        candidates_serializer = CandidateSerializer(candidates, many=True)
        data = serializer.data
        data['candidates'] = candidates_serializer.data
        return Response(data, status=status.HTTP_200_OK)
    if request.method == 'DELETE':
        vacancy.delete()
        return Response({'message': 'Request deleted'}, status=status.HTTP_404_NOT_FOUND)

