from rest_framework import serializers
from properties.serializers import HardSkillSerializer
from users.serializers import CompanySerializer
from vacancies.models import Vacancy


class VacancySerializer(serializers.ModelSerializer):
    hard_skill = HardSkillSerializer(read_only=True, many=True)
    company = CompanySerializer(read_only=True)
    created_at = serializers.DateField(format="%B %d %Y")

    class Meta:
        model = Vacancy
        fields = [
            'id',
            'company',
            'hard_skill',
            'start_date',
            'occupation',
            'level',
            'description',
            'created_at'
        ]
