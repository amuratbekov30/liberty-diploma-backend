import stripe
from django.contrib import admin
from common.variables import PRICES
from vacancies.models import Vacancy, Subscription


class SubscriptionInline(admin.TabularInline):
    model = Subscription
    extra = 0


class VacancyAdmin(admin.ModelAdmin):
    model = Vacancy
    list_display = (
        'company',
        'level',
        'start_date',
        'created_at',
    )
    list_filter = (
        'level',
    )
    search_fields = ('company', )
    inlines = [SubscriptionInline]


class SubscriptionAdmin(admin.ModelAdmin):
    model = Subscription
    list_display = (
        'vacancy',
        'status',
        'subscription_id',
    )
    list_filter = (
        'status',
    )
    search_fields = ('subscription_id', )

    def save_model(self, request, obj, form, change):
        subscription = stripe.Subscription.create(
            customer=obj.vacancy.company.customer_id,
            items=[{'price': PRICES[obj.vacancy.level][obj.vacancy.occupation]}],
            payment_behavior='default_incomplete',
            payment_settings={
                'save_default_payment_method': 'on_subscription',
            },
        )
        obj.subscription_id = subscription.id
        super().save_model(request, obj, form, change)


admin.site.register(Vacancy, VacancyAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
