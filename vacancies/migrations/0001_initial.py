# Generated by Django 4.0.5 on 2022-07-01 07:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('properties', '0002_delete_experience'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Vacancy',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('specialist', models.CharField(choices=[('PPC', 'PPC'), ('Digital', 'Digital')], max_length=10, verbose_name='Специалист')),
                ('start_date', models.DateField(blank=True, null=True, verbose_name='Дата начала')),
                ('salary', models.FloatField(blank=True, null=True, verbose_name='Зарплата')),
                ('level', models.CharField(choices=[('Junior', 'Junior'), ('Middle', 'Middle'), ('Senior', 'Senior')], max_length=15, verbose_name='Уровень')),
                ('description', models.CharField(max_length=500, verbose_name='Описание')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Компания')),
                ('hard_skill', models.ManyToManyField(to='properties.hardskill', verbose_name='Проф навык')),
            ],
            options={
                'verbose_name': 'Вакансия',
                'verbose_name_plural': 'Вакансии',
            },
        ),
    ]
