from django.urls import path
from vacancies.views import vacancies_list, vacancy_detail

urlpatterns = [
    path('', vacancies_list, name='vacancies_list'),
    path('<int:pk>/', vacancy_detail, name='vacancy_detail'),

]