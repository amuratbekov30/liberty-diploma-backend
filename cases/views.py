from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from cases.models import Case
from cases.serializers import CaseSerializer


@api_view(['GET'])
@permission_classes((AllowAny,))
def cases_list(request):
    if request.method == 'GET':
        cases = Case.objects.all().order_by('-created_at')
        serializer = CaseSerializer(cases, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def cases_list_top(request):
    if request.method == 'GET':
        cases_top = Case.objects.all().order_by('-created_at')[:3]
        serializer = CaseSerializer(cases_top, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def cases_detail(request):
    case_name = request.query_params.get('title')
    try:
        case = Case.objects.get(title=case_name)
    except Case.DoesNotExist:
        return Response({'message': 'Case not found'}, status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = CaseSerializer(case)
        return Response(serializer.data, status=status.HTTP_200_OK)
