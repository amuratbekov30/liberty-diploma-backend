from django.db import models

from users.models import User


class Case(models.Model):
    title = models.CharField(
        max_length=250,
        verbose_name='Заголовок'
    )
    author = models.ForeignKey(
        User,
        verbose_name='Автор',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    image = models.FileField(
        verbose_name='Картинка',
        upload_to='casecontent/problems/',
        null=True,
        blank=True
    )
    challenge = models.TextField(
        verbose_name='challenge',
        null=True,
        blank=True
    )
    my_role = models.TextField(
        verbose_name='My Role',
        null=True,
        blank=True
    )
    solution = models.TextField(
        verbose_name='solution',
        null=True,
        blank=True
    )
    specific_goals = models.TextField(
        verbose_name='specific_goals',
        null=True,
        blank=True
    )
    description = models.TextField(
        verbose_name='Описание',
        null=True,
        blank=True
    )
    # Дата создания
    created_at = models.DateField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )

    class Meta:
        verbose_name = 'Кейс'
        verbose_name_plural = 'Кейсы'

    def __str__(self):
        return self.title


