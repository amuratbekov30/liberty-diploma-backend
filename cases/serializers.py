from rest_framework import serializers
from cases.models import Case
from users.serializers import ShortDesignerSerializer


class CaseSerializer(serializers.ModelSerializer):
    created_at = serializers.DateField(format="%B %d %Y")
    author = ShortDesignerSerializer(read_only=True)

    class Meta:
        model = Case
        fields = [
            'id',
            'author',
            'title',
            'image',
            'challenge',
            'my_role',
            'solution',
            'specific_goals',
            'description',
            'created_at',
        ]
