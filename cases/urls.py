from django.urls import path
from cases.views import cases_list, cases_list_top, cases_detail

urlpatterns = [
    path('', cases_list, name='cases_list'),
    path('top/', cases_list_top, name='cases_list_top'),
    path('case/', cases_detail, name='cases_detail'),
]
