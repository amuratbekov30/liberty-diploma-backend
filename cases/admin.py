from django.contrib import admin
from cases.models import Case


class CaseAdmin(admin.ModelAdmin):
    model = Case
    list_display = ('title', 'image', 'created_at')
    search_fields = ('title', )


admin.site.register(Case, CaseAdmin)


