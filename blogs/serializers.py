from rest_framework import serializers
from blogs.models import Article


class ArticleSerializer(serializers.ModelSerializer):
    created_at = serializers.DateField(format="%B %d %Y")

    class Meta:
        model = Article
        fields = [
            'id',
            'title',
            'text_1',
            'image_1',
            'text_2',
            'image_2',
            'text_3',
            'image_3',
            'text_4',
            'created_at',
        ]


class ShortArticleSerializer(serializers.ModelSerializer):
    created_at = serializers.DateField(format="%B %d %Y")

    class Meta:
        model = Article
        fields = [
            'id',
            'title',
            'image_1',
            'created_at',
        ]
