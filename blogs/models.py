from django.db import models


class Article(models.Model):
    title = models.CharField(
        max_length=250,
        verbose_name='Заголовок'
    )
    text_1 = models.TextField(
        verbose_name='Текст_1',
        null=True,
        blank=True
    )
    image_1 = models.FileField(
        verbose_name='Картинка_1',
        upload_to='blogcontent/',
        null=True,
        blank=True
    )
    text_2 = models.TextField(
        verbose_name='Текст_2',
        null=True,
        blank=True
    )
    image_2 = models.FileField(
        verbose_name='Картинка_2',
        upload_to='blogcontent/',
        null=True,
        blank=True
    )
    text_3 = models.TextField(
        verbose_name='Текст_3',
        null=True,
        blank=True
    )
    image_3 = models.FileField(
        verbose_name='Картинка_3',
        upload_to='blogcontent/',
        null=True,
        blank=True
    )
    text_4 = models.TextField(
        verbose_name='Текст_3',
        null=True,
        blank=True
    )
    # Дата создания
    created_at = models.DateField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )

    class Meta:
        verbose_name = 'Блог'
        verbose_name_plural = 'Блоги'

    def __str__(self):
        return self.title


