from django.contrib import admin
from blogs.models import Article


class BlogAdmin(admin.ModelAdmin):
    model = Article
    list_display = ('title', 'image_1')
    search_fields = ('title', )


admin.site.register(Article, BlogAdmin)


