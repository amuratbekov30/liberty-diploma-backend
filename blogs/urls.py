from django.urls import path
from blogs.views import articles_list, articles_list_top, articles_detail

urlpatterns = [
    path('', articles_list, name='articles_list'),
    path('top/', articles_list_top, name='articles_list_top'),
    path('blog/', articles_detail, name='articles_article'),
]
