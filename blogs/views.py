from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from blogs.models import Article
from blogs.serializers import ArticleSerializer


@api_view(['GET'])
@permission_classes((AllowAny,))
def articles_list(request):
    if request.method == 'GET':
        articles = Article.objects.all().order_by('-created_at')
        serializer = ArticleSerializer(articles, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def articles_list_top(request):
    if request.method == 'GET':
        articles_top = Article.objects.all().order_by('-created_at')[:3]
        serializer = ArticleSerializer(articles_top, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def articles_detail(request):
    article_name = request.query_params.get('title')
    try:
        article = Article.objects.get(title=article_name)
    except Article.DoesNotExist:
        return Response({'message': 'Article not found'}, status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = ArticleSerializer(article)
        return Response(serializer.data, status=status.HTTP_200_OK)
