from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from properties.models import SoftSkill, HardSkill, Country


class UserManager(BaseUserManager):
    def create_user(self, username, password=None, **extra_fields):
        if not username:
            raise ValueError('Please, type username')

        user = self.model(
            username=username,
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        user = self.create_user(
            username=username,
            password=password,
            **extra_fields
        )

        if extra_fields.get('is_staff') is not True:
            raise ValueError('is_staff=True required for Superuser')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('is_superuser=True required for Superuser')

        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(
        max_length=100,
        verbose_name='Имя',
        null=True,
        blank=True
    )
    last_name = models.CharField(
        max_length=100,
        verbose_name='Фамилия',
        null=True,
        blank=True
    )
    position = models.CharField(
        max_length=100,
        verbose_name='Род деятельности',
        null=True,
        blank=True
    )
    customer_id = models.CharField(
        max_length=100,
        verbose_name='Customer_id',
        null=True,
        blank=True
    )
    description = models.TextField(
        max_length=800,
        verbose_name='Описание',
        null=True,
        blank=True
    )
    username = models.CharField(
        max_length=256,
        verbose_name='Почта',
        unique=True,
    )
    banners_count = models.IntegerField(
        verbose_name='Количество выполненных работ',
        default=0
    )
    experience_years = models.IntegerField(
        verbose_name='Время работы',
        default=0
    )
    experience_designer_years = models.IntegerField(
        verbose_name='дизайнером',
        default=0
    )
    avatar = models.ImageField(
        verbose_name='Аватар',
        upload_to="avatars/",
        null=True,
        blank=True
    )
    site = models.CharField(
        verbose_name='Сайт',
        max_length=250,
        null=True,
        blank=True
    )
    company_name = models.CharField(
        verbose_name='Название компании',
        max_length=250,
        null=True,
        blank=True
    )
    phone_number = models.CharField(
        verbose_name='Номер телефона',
        max_length=18,
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )

    # Дата редактирования
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name='Дата редактирования',
        null=True,
    )
    is_first = models.BooleanField(
        default=True,
        verbose_name='Еще не входил?',
    )
    # Django Additional (не трогать)
    is_staff = models.BooleanField(
        default=False,
        verbose_name='Сотрудник организации',
        help_text='Открывает доступ к панели администратора с указанными правами',
    )  # права к админ-панели
    is_active = models.BooleanField(
        default=True,
        verbose_name='Активный аккаунт',
        help_text='Статус (по умолчанию "Да")',
    )  # активный аккаунт

    USERNAME_FIELD = 'username'

    objects = UserManager()

    class Meta:
        ordering = ('-created_at',)
        verbose_name = 'Пользователь системы'
        verbose_name_plural = 'Пользователи системы'

    def __str__(self):
        return self.username


class Experience(models.Model):
    user = models.ForeignKey(
        'User',
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    value = models.CharField(
        max_length=100,
        verbose_name='Фамилия',
        null=True,
        blank=True
    )
    description = models.CharField(
        max_length=200,
        verbose_name='Описание скила',
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'Краткий опыт'
        verbose_name_plural = 'Краткие опыты'

    def __str__(self):
        return f'{self.id}'


class OwnSkill(models.Model):
    user = models.ForeignKey(
        'User',
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    skill = models.CharField(
        max_length=100,
        verbose_name='Умения',
    )

    class Meta:
        verbose_name = 'Скилл'
        verbose_name_plural = 'Скиллы'

    def __str__(self):
        return f'{self.id}'


class Ticket(models.Model):
    def validate_image(fieldfile_obj):
        filesize = fieldfile_obj.size
        megabyte_limit =1.0
        if filesize > megabyte_limit * 1024 * 1024:
            raise ValidationError("Max file size is %sMB" % str(megabyte_limit))

    STATUS_CHOICES = [
        ('Waiting', 'Waiting'),
        ('Closed', 'Closed'),
    ]
    user = models.ForeignKey(
        'User',
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    email = models.CharField(
        max_length=120,
        verbose_name='Почта',
    )
    name = models.CharField(
        max_length=120,
        verbose_name='Имя',
    )
    text = models.TextField(
        max_length=500,
        verbose_name='Сообщение',
    )
    title = models.CharField(
        max_length=150,
        verbose_name='Заголовок',
    )
    file = models.FileField(
        verbose_name='Файл',
        upload_to='ticket/',
        validators=[validate_image],
        null=True,
        blank=True
    )
    status = models.CharField(
        verbose_name='Статус',
        max_length=10,
        default='Waiting',
        choices=STATUS_CHOICES
    )
    # Дата создания
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )

    class Meta:
        verbose_name = 'Тикет'
        verbose_name_plural = 'Тикеты'

    def __str__(self):
        return f'{self.id}'


class Notification(models.Model):
    user = models.ForeignKey(
        'User',
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    text = models.TextField(
        max_length=500,
        verbose_name='Сообщение',
    )
    title = models.CharField(
        max_length=150,
        verbose_name='Заголовок',
    )
    date = models.DateTimeField(
        verbose_name='Дата',
        auto_now=False,
        auto_now_add=False,
    )
    viewed = models.BooleanField(
        verbose_name="Прочитано",
        default=False
    )

    class Meta:
        verbose_name = 'Уведомление'
        verbose_name_plural = 'Уведомления'

    def __str__(self):
        return f'{self.id}'


class Call(models.Model):
    email = models.CharField(
        max_length=255,
        verbose_name='Почта'
    )
    phone = models.CharField(
        max_length=255,
        verbose_name='Телефон'
    )
    name = models.CharField(
        max_length=255,
        verbose_name='Почта'
    )
    message = models.TextField(
        verbose_name='Сообщение'
    )
    # Дата создания
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )

    class Meta:
        verbose_name = 'Заявка с сайта'
        verbose_name_plural = 'Заявки с сайта'

    def __str__(self):
        return f"{self.email}"

