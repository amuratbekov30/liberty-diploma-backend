from django.urls import path
from users.views import (
    ticket_create, notifications_list, me_view, tickets_list, ticket_detail,
    if_first_update, brief_create, profile_update, update_names, get_designer, get_all_designers, book_call, register,
    upload_file, work_avatar, load_attachment, update_own_skills, create_work, update_photo, update_profile,
    update_work_status, update_work_comment
)

urlpatterns = [
    path('me/', me_view, name='me_view'),
    path('is_first/', if_first_update, name='if_first_update'),
    path('notifications/', notifications_list, name='notifications_list'),

    path('tickets/', tickets_list, name='tickets_list'),
    path('tickets/<int:ticket_id>/', ticket_detail, name='ticket_detail'),
    path('tickets/new/', ticket_create, name='ticket_create'),

    path('brief/create/', brief_create, name='brief_create'),
    path('profile/', profile_update, name='brief_create_update'),
    path('update_names/', update_names, name='update_names'),
    # new paths
    path('designer/<int:user_id>/', get_designer, name='get_designer'),
    path('designer/all/', get_all_designers, name='get_all_designers'),
    path('book_call/', book_call, name='book_call'),
    path('register/', register, name='register'),
    path('upload_file/<int:work_id>/', upload_file, name='upload_file'),
    path('work_avatar/<int:work_id>/', work_avatar, name='work_avatar'),
    path('load_attachment/<int:work_id>/', load_attachment, name='load_attachment'),
    path('update_own_skills/', update_own_skills, name='update_own_skills'),
    path('create_work/', create_work, name='create_work'),
    path('update_photo/', update_photo, name='update_photo'),
    path('update_work_status/<int:work_id>/', update_work_status, name='update_work_status'),
    path('update_work_comment/<int:work_id>/', update_work_comment, name='update_work_comment')
]
