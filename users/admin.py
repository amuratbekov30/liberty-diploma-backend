from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from users.models import (
    User, Ticket, Notification, Experience, OwnSkill, Call
)
from django.contrib.auth.models import Group

from vacancies.models import Subscription


class NotificationInline(admin.TabularInline):
    model = Notification
    extra = 0


class ExperienceInline(admin.TabularInline):
    model = Experience
    extra = 0


class OwnSkillInline(admin.TabularInline):
    model = OwnSkill
    extra = 0


# Админка пользователей
class UserCustomAdmin(UserAdmin):
    model = User
    list_display = (
        'username',
        'first_name',
        'last_name',
        'customer_id',
        'avatar',
        'phone_number',
        'company_name',
        'site',
        'created_at',

    )
    list_filter = (
        'is_staff',
        'is_active',
    )
    search_fields = (
        'username',
        'company_name'
    )
    fieldsets = (
        ('Основное', {
            'fields': (
                'username',
                'first_name',
                'last_name',
                'customer_id',
                'position',
                'avatar',
                'password',
                'phone_number',
                'company_name',
                'site',
                'is_first',
            )
        }),
        ('Дополнительная информация', {
            'classes': ('collapse', ),
            'fields': ('is_staff', 'is_active', 'created_at', 'updated_at', )
        })
    )
    add_fieldsets = (
        ('Основное', {
            'classes': ('wide', ),
            'fields': (
                'username',
                'password1',
                'password2',
                'first_name',
                'last_name',
                'phone_number',
                'company_name',
                'site',
                'description',
            )
        }),
        ('Дополнительная информация', {
            'classes': ('collapse', ),
            'fields': ('is_staff', 'is_active', )
        })
    )
    readonly_fields = ('created_at', 'updated_at', 'last_login', )
    ordering = ('created_at', )
    inlines = [NotificationInline, ExperienceInline, OwnSkillInline]


class TicketAdmin(admin.ModelAdmin):
    model = Ticket
    list_display = (
        'id',
        'user',
        'email',
        'name',
        'title',
        'created_at'
    )
    list_filter = (
        'status',
    )
    search_fields = ('title', )


class CallAdmin(admin.ModelAdmin):
    model = Call
    list_display = ('email', 'name', 'phone', 'message', 'created_at')
    search_fields = ('email', )


admin.site.register(User, UserCustomAdmin)
admin.site.register(Ticket, TicketAdmin)
admin.site.unregister(Group)
admin.site.register(Call, CallAdmin)

# Настройки Хедеров в Админке
admin.site.site_header = 'Панель администратора'
admin.site.site_title = 'Админ-панель Liberty'
admin.site.index_title = 'Liberty Admin'

