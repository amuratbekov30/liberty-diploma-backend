import os

import stripe
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from django.db.models import Avg
import requests
from common.create_password import create_password
from common.send_email import send_email
from properties.models import HardSkill
from users.models import User, Ticket, Call, OwnSkill
from users.serializers import TicketSerializer, NotificationSerializer, CompanySerializer, DesignerSerializer
from vacancies.models import Vacancy
from works.models import Work, Attachment

import zipfile


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def me_view(request):
    serializer = CompanySerializer(request.user)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def if_first_update(request):
    user = request.user
    user.is_first = False
    user.save()
    return Response(status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def notifications_list(request):
    serializer = NotificationSerializer(request.user.notification_set.all(), many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def ticket_create(request):
    if request.method == 'POST':
        serializer = TicketSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(
                {"message": f"Your application number №{serializer.data['id']}. Wait for the manager's response"},
                status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def tickets_list(request):
    status_t = request.query_params.get('status')
    if status_t:
        serializer = TicketSerializer(request.user.ticket_set.filter(user=request.user, status=status_t), many=True)
    else:
        serializer = TicketSerializer(request.user.ticket_set.filter(user=request.user), many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def ticket_detail(request, ticket_id):
    try:
        ticket = Ticket.objects.get(id=ticket_id, user=request.user)
    except Ticket.DoesNotExist:
        return Response({'message': "Application not found or you don't have access"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TicketSerializer(ticket)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def brief_create(request):
    if request.method == 'POST':
        email = request.data['email'].strip()
        user = User.objects.filter(username=email).first()
        if not user:
            user = User(
                username=email,
                company_name=request.data['company'].strip(),
                site=request.data['website'].strip(),
            )
            user.save()
            password = create_password()
            user.set_password(password)
            user.save()
            send_email(
                'Welcome to Liberty.space!',
                'Welcome to Liberty.space!\n'
                'Thank you for joining us!\n'
                'Here are the email and password to access your profile.\n'
                f'Your email: {request.data["email"]}\n'
                f'Your password:{password}\n',
                [f'{request.data["email"]}'])
            # send_email(
            #     'Welcome to Liberty.space!',
            #     f'Login: {request.data["email"]}\nPassword: {password}',
            #     [f'{request.data["email"]}'])
        if not user.customer_id:
            customer = stripe.Customer.create(email=user.username)
            user.customer_id = customer.id
            user.save()
        if not request.data['job_description']:
            job_file = None
            description = None
        elif isinstance(request.data['job_description'], str):
            description = request.data['job_description'].strip()
            job_file = None
        else:
            job_file = request.data['job_description']
            description = ''
        vacancy = Vacancy(
            company=user,
            start_date=request.data['start_date'],
            occupation=request.data['occupation'],
            level=request.data['level'],
            description=description,
            file=job_file,
        )
        vacancy.save()
        for key, value in request.data['skills'].items():
            for skill in value:
                hard_skill = HardSkill.objects.filter(id=skill['id']).first()
                vacancy.hard_skill.add(hard_skill)
        token, _ = Token.objects.get_or_create(user=user)
        return Response({"token": token.key}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def profile_update(request):
    if request.method == 'POST':
        serializer = CompanySerializer(request.user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {"message": f"Profile updated"},
                status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_names(request):
    if request.method == 'POST':
        first_name = request.data['first_name']
        last_name = request.data['last_name']
        phone_number = request.data['phone_number']
        User.objects.filter(id=request.user.id).update(first_name=first_name, last_name=last_name, phone_number=phone_number)
        return Response({"message": "profile updated"}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_designer(request, user_id):
    user = User.objects.filter(id=user_id).first()
    users = User.objects.filter(position__isnull=False).exclude(id=user_id)
    serializer = DesignerSerializer(user)
    serializer_all = DesignerSerializer(users, many=True)
    return Response({"designer": serializer.data, "others": serializer_all.data}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_all_designers(request):
    users = User.objects.filter(position__isnull=False)
    serializer_all = DesignerSerializer(users, many=True)
    return Response(serializer_all.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def book_call(request):
    if request.method == 'POST':
        email = request.data['email']
        name = request.data['name']
        phone = request.data['phone']
        text_message = request.data['message']
        call = Call(email=email, name=name, phone=phone, message=text_message)
        tg_token = "6237300280:AAEJsQEP0ofdVWVmDUVPF1VjElZl839DgZs"
        chat_id = "-1001722441567"
        message = f'new request from website test.liberty.space\n {name} \n {email} \n {phone} \n {text_message}'
        url = f"https://api.telegram.org/bot{tg_token}/sendMessage?chat_id={chat_id}&text={message}"
        print(requests.get(url).json())
        call.save()
        return Response('call_created', status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def register(request):
    if request.method == 'POST':
        first_name = request.data['first_name']
        last_name = request.data['last_name']
        phone_number = request.data['phone_number']
        username = request.data['email']
        password = request.data['password']
        is_staff = request.data['is_staff']
        user_exists = User.objects.filter(username=username).first()
        if user_exists:
            return Response({"message": "user already exists"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            user = User(
                first_name=first_name,
                is_staff=is_staff,
                last_name=last_name,
                phone_number=phone_number,
                username=username
            )
            user.set_password(password)
            user.save()
            token, _ = Token.objects.get_or_create(user=user)
            return Response({"token": token.key}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_profile(request):
    if request.method == 'POST':
        position = request.data['position']
        user = User.objects.filter(id=request.user.id).first()
        if position == 'company':
            user.company_name = request.data['company_name']
            user.site = request.data['site']
            user.description = request.data['description']
            user.phone_number = request.data['phone_number']
            user.save()
        else:
            user.description = request.data['description']
            user.banners_count = request.data['banners_count']
            user.experience_years = request.data['experience_years']
            user.experience_designer_years = request.data['experience_designer_years']
            user.save()
    return Response({"message": "profile updated"}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_photo(request):
    if request.method == 'POST':
        user = User.objects.filter(id=request.user.id).first()
        user.avatar = request.data['file']
        user.save()
        return Response({"message": "avatar updated"}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def create_work(request):
    if request.method == 'POST':
        user = request.user
        name = request.data['name']
        industry = request.data['industry']
        category = request.data['category']
        cost = request.data['cost']
        resize_cost = request.data['resize_cost']
        description = request.data['description']
        if user.is_staff:
            work = Work(
                name=name,
                status='Waiting',
                designer=request.user,
                industry=industry,
                category=category,
                cost=cost,
                resize_cost=resize_cost,
                description=description
            )
            work.save()
            return Response({"work_id": work.id}, status=status.HTTP_200_OK)
        else:
            work = Work(
                name=name,
                status='To Do',
                customer=request.user,
                industry=industry,
                category=category,
                cost=cost,
                resize_cost=resize_cost,
                description=description
            )
            work.save()
            return Response({"work_id": work.id}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def load_attachment(request, work_id):
    work = Work.objects.filter(id=work_id).first()
    file_upload = request.data['attachment']
    attachment = Attachment(
        work=work,
        file=file_upload
    )
    attachment.save()
    return Response({"attachment": 'attachment created'}, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def upload_file(request, work_id):
    if request.method == 'POST':
        work = Work.objects.filter(id=work_id).first()
        file_upload = request.data['file']
        if work.file:
            location = f'/home/liberty-diploma-backend{work.file}'
            if os.path.exists(location):
                os.remove(location)
        work.file = file_upload
        work.save()
        file_name = os.path.basename(f'{work.file}')
        last_dot_index = len(file_name) - file_name.rfind('.')
        if work.category.name == 'Rich media' and '.zip' in file_name:
            zip_path = f'{work.file}'
            extract_dir = 'content/'
            with zipfile.ZipFile(zip_path, 'r') as zip_ref:
                zip_ref.extractall(extract_dir)
            work.rich_link = f'/content/{file_name[:-last_dot_index]}/index.html'
            work.save()
        return Response({"message": work.rich_link, "work_name": f'{work.file}'}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def work_avatar(request, work_id):
    if request.method == 'POST':
        work = Work.objects.filter(id=work_id).first()
        file_upload = request.data['file']
        work.avatar = file_upload
        work.save()
        return Response({"message": "work avatar uploaded"}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_own_skills(request):
    if request.method == 'POST':
        skills = request.data['skills']
        for skill in skills:
            own_skill = OwnSkill(
                user=request.user,
                skill=skill
            )
            own_skill.save()
        return Response({"message": "skills updated"}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_work_status(request, work_id):
    if request.method == 'POST':
        work = Work.objects.filter(id=work_id).first()
        if work.status == 'To Do':
            work.designer = request.user
        work.status = request.data['status']
        work.save()
        if request.data['status'] == 'Accepted':
            tg_token = "6237300280:AAEJsQEP0ofdVWVmDUVPF1VjElZl839DgZs"
            chat_id = "-1001722441567"
            message = f'new request from website test.liberty.space\n {work.name} \n {work.status} \n ' \
                      f'please create invoice'
            url = f"https://api.telegram.org/bot{tg_token}/sendMessage?chat_id={chat_id}&text={message}"
            print(requests.get(url).json())
        return Response({"message": "work status updated"}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_work_comment(request, work_id):
    if request.method == 'POST':
        work = Work.objects.filter(id=work_id).first()
        work.comment = request.data['comment']
        work.save()
        return Response({"message": "work comment updated"}, status=status.HTTP_200_OK)














