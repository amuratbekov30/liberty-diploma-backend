from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from users.models import User, Ticket, Notification, Experience, OwnSkill
from works.models import Work


class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = [
            'id',
            'email',
            'name',
            'text',
            'title',
            'file',
            'status',
            'created_at'
        ]


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = [
            'id',
            'text',
            'title',
            'date',
        ]


class CompanySerializer(serializers.ModelSerializer):
    notification_set = SerializerMethodField('get_notification_set', read_only=True)

    def get_notification_set(self, instance):
        notification_set = instance.notification_set.filter(viewed=False).order_by('id')
        return NotificationSerializer(notification_set, many=True).data

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'company_name',
            'site',
            'password',
            'first_name',
            'last_name',
            'avatar',
            'phone_number',
            'is_first',
            'notification_set',
            'is_staff',
            'banners_count',
            'experience_years',
            'experience_designer_years',
            'description',
            'position'
        ]


class ExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        fields = [
            'value',
            'description',
        ]


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = OwnSkill
        fields = [
            'skill',
        ]


class DesignerSerializer(serializers.ModelSerializer):
    experience_set = ExperienceSerializer(read_only=True, many=True)
    ownskill_set = SkillSerializer(read_only=True, many=True)
    portfolio = serializers.SerializerMethodField('get_works', read_only=True)

    def get_works(self, instance):
        work_gallery = []
        works = Work.objects.filter(designer=instance).order_by('created_at')
        for work in works:
            work_gallery.append(f'{work.avatar}')
        return work_gallery

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'avatar',
            'first_name',
            'last_name',
            'experience_set',
            'description',
            'ownskill_set',
            'portfolio',
            'position',
            'banners_count',
            'experience_years',
            'experience_designer_years',
        ]


class ShortDesignerSerializer(serializers.ModelSerializer):
    ownskill_set = SkillSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'avatar',
            'first_name',
            'last_name',
            'position',
            'ownskill_set',
            'banners_count',
            'experience_years',
            'experience_designer_years',
        ]

