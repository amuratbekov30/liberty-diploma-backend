from django.urls import path
from candidates.views import (
    top_four_candidate_list, candidates_list
)

urlpatterns = [
    # path('top/', top_four_candidate_list, name='top_four_candidate_list'),
    path('', candidates_list, name='candidates_list'),
]