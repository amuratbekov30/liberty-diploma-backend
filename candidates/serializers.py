from datetime import datetime

from django.db.models import Avg
from rest_framework import serializers

from candidates.models import Candidate, Experience, CandidateHard, CandidateSoft, Brand
from properties.serializers import CountrySerializer, HardSkillSerializer, SoftSkillSerializer


class ExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        fields = [
            'id',
            'position',
            'company_name',
            'start_date',
            'end_date',
            'description',
        ]


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = [
            'id',
            'company_name',
            'logo',
        ]


class CandidateHardSerializer(serializers.ModelSerializer):
    hard_skill = HardSkillSerializer(read_only=True)

    class Meta:
        model = CandidateHard
        fields = [
            'id',
            'hard_skill',
            'score',
            'answers_url',
        ]


class CandidateSoftSerializer(serializers.ModelSerializer):
    soft_skill = SoftSkillSerializer(read_only=True)

    class Meta:
        model = CandidateSoft
        fields = [
            'id',
            'soft_skill',
            'score',
        ]


class CandidateSerializer(serializers.ModelSerializer):
    country = CountrySerializer(read_only=True)
    candidatesoft_set = CandidateSoftSerializer(read_only=True, many=True)
    brand_set = BrandSerializer(read_only=True, many=True)
    experience_set = ExperienceSerializer(read_only=True, many=True)
    rating = serializers.SerializerMethodField('count_rating')
    candidatehard_set = serializers.SerializerMethodField('get_candidatehard_set')
    experience_age = serializers.SerializerMethodField('count_experience')
    birthday = serializers.DateField(format="%m-%d-%Y")

    def get_candidatehard_set(self, instance):
        candidatehard_set = instance.candidatehard_set.all().order_by('-score')
        return CandidateHardSerializer(candidatehard_set, many=True).data

    def count_rating(self, user):
        return user.candidatehard_set.all().aggregate(a=Avg('score'))['a']

    def count_experience(self, user):
        exp = user.experience_set.all().order_by('start_date').first()
        if exp:
            return int((datetime.now().date() - exp.start_date).days / 365)
        else:
            return 0

    class Meta:
        model = Candidate
        fields = [
            'id',
            'first_name',
            'last_name',
            'avatar',
            'country',
            'birthday',
            'gender',
            'description',
            'candidatehard_set',
            'candidatesoft_set',
            'experience_set',
            'rating',
            'experience_age',
            'fake_candidate',
            'brand_set'
        ]
