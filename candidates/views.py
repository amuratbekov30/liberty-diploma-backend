from django.db.models import Avg
from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from candidates.models import Candidate
from candidates.serializers import CandidateSerializer


@api_view(['GET'])
@permission_classes((AllowAny,))
def top_four_candidate_list(request):
    if request.method == 'GET':
        candidates = Candidate.objects.all().annotate(
            avg_score=Avg('candidatehard__score')
        ).order_by('-avg_score')
        serializer = CandidateSerializer(candidates, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def candidates_list(request):
    if request.method == 'GET':
        level = request.query_params.get('level')
        fake = request.query_params.get('fake')
        if level not in ['Junior', 'Middle', 'Senior']:
            level = "Junior"
        if fake:
            candidates = Candidate.objects.filter(level=level, fake_candidate__isnull=False).annotate(
                avg_score=Avg('candidatehard__score')
            ).order_by('-avg_score')
        else:
            candidates = Candidate.objects.filter(level=level).annotate(
                avg_score=Avg('candidatehard__score')
            ).order_by('-avg_score')
        serializer = CandidateSerializer(candidates, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
