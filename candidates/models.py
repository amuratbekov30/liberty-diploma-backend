from django.db import models

from properties.models import Country, HardSkill, SoftSkill


class Candidate(models.Model):
    GENDER_OPTIONS = [
        ('Man', 'Man'),
        ('Woman', 'Woman')
    ]
    LEVEL_OPTIONS = [
        ('Junior', 'Junior'),
        ('Middle', 'Middle'),
        ('Senior', 'Senior')
    ]
    level = models.CharField(
        verbose_name='Уровень',
        choices=LEVEL_OPTIONS,
        max_length=15,
        default='Junior'
    )
    first_name = models.CharField(
        max_length=100,
        verbose_name='Имя',
        null=True,
        blank=True
    )
    last_name = models.CharField(
        max_length=100,
        verbose_name='Фамилия',
        null=True,
        blank=True
    )
    avatar = models.ImageField(
        verbose_name='Аватар',
        upload_to="avatars/",
        null=True,
        blank=True
    )
    country = models.ForeignKey(
        Country,
        verbose_name='Страна',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    birthday = models.DateField(
        verbose_name='Дата рождения',
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True
    )
    gender = models.CharField(
        verbose_name='Пол',
        choices=GENDER_OPTIONS,
        max_length=10,
        null=True,
        blank=True
    )
    description = models.CharField(
        max_length=2000,
        verbose_name='Обо мне',
        null=True,
        blank=True
    )
    fake_candidate = models.CharField(
        max_length=60,
        verbose_name='Фейковое название кандидата',
        help_text='Если это фейковый кандидат для лендинга то заполните это поле',
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Кандидат'
        verbose_name_plural = 'Кандидаты'

    def __str__(self):
        return f"{self.id}"


class CandidateSoft(models.Model):
    candidate = models.ForeignKey(
        Candidate,
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    soft_skill = models.ForeignKey(
        SoftSkill,
        on_delete=models.CASCADE,
        verbose_name='Гибкий навык',
    )
    score = models.FloatField(
        verbose_name='Оценка',
        null=True
    )

    class Meta:
        verbose_name = 'Гибкий навык кандидата'
        verbose_name_plural = 'Гибкие навыки кандидата'
        ordering = ('soft_skill__order', 'id')

    def __str__(self):
        return self.soft_skill.name


class CandidateHard(models.Model):
    candidate = models.ForeignKey(
        Candidate,
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    hard_skill = models.ForeignKey(
        HardSkill,
        on_delete=models.CASCADE,
        verbose_name='Проф навык',
    )
    score = models.FloatField(
        verbose_name='Оценка',
        null=True,
        default=0
    )
    answers_url = models.CharField(
        max_length=256,
        verbose_name='Ссылка на ответы',
        blank=True,
        default=''
    )

    class Meta:
        verbose_name = 'Проф навык кандидата'
        verbose_name_plural = 'Проф навыки кандидата'

    def __str__(self):
        return self.hard_skill.name


class Experience(models.Model):
    candidate = models.ForeignKey(
        Candidate,
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    position = models.CharField(
        max_length=100,
        verbose_name='Должность'
    )
    company_name = models.CharField(
        max_length=100,
        verbose_name='Название компании'
    )
    start_date = models.DateField(
        verbose_name='Дата начала',
        auto_now=False,
        auto_now_add=False,
    )
    end_date = models.DateField(
        verbose_name='Дата конца',
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True
    )
    description = models.CharField(
        max_length=500,
        verbose_name='Описание'
    )

    class Meta:
        verbose_name = 'Опыт работы кандидата'
        verbose_name_plural = 'Опыты работы кандидата'

    def __str__(self):
        return self.position


class Brand(models.Model):
    candidate = models.ForeignKey(
        Candidate,
        verbose_name='Пользователь',
        on_delete=models.CASCADE,
    )
    company_name = models.CharField(
        max_length=100,
        verbose_name='Название компании'
    )
    logo = models.ImageField(
        verbose_name='Логотип компании',
        upload_to="brands/",
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'

    def __str__(self):
        return self.company_name
