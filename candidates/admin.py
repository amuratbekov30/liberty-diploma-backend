from django.contrib import admin
from candidates.models import Experience, CandidateSoft, CandidateHard, Candidate, Brand


class ExperienceInline(admin.TabularInline):
    model = Experience
    extra = 0


class UserSoftInline(admin.TabularInline):
    model = CandidateSoft
    extra = 0


class UserHardInline(admin.TabularInline):
    model = CandidateHard
    extra = 0


class BrandInline(admin.TabularInline):
    model = Brand
    extra = 0


class CandidateAdmin(admin.ModelAdmin):
    model = Candidate
    list_display = (
        'id',
        'first_name',
        'last_name',
        'avatar',
        'level',
        'country',
        'birthday',
        'gender',
        'fake_candidate'
    )
    list_filter = (
        'gender',
        'country',
    )
    inlines = [UserHardInline, UserSoftInline, BrandInline, ExperienceInline]


admin.site.register(Candidate, CandidateAdmin)

