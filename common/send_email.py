from django.core.mail import send_mail
from project import settings


def send_email(subject: str, message: str, recipient_list: list):
    send_mail(subject, message, settings.EMAIL_HOST_USER, recipient_list)
    return 'message successfully sent'
