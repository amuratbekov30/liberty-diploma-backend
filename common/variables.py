DOMAIN = 'http://liberty.space/dashboard/'
PRICES = {
    "Junior": {
        "10 hours per week": "price_1LXNVIIeIfx6Xkag0SlpmaQE",
        "20 hours per week": "price_1LXNVIIeIfx6Xkaghgs0dA8v",
        "40 hours per week": "price_1LXNVIIeIfx6XkagDgRvMPwL",
    },
    "Middle": {
        "10 hours per week": "price_1LX13kIeIfx6Xkag59zFYDi7",
        "20 hours per week": "price_1LXNSxIeIfx6XkagyqzI9BLo",
        "40 hours per week": "price_1LXNSxIeIfx6Xkag1TRPmE78",
    },
    "Senior": {
        "10 hours per week": "price_1LX17sIeIfx6Xkag9u0HEIsc",
        "20 hours per week": "price_1LXNTxIeIfx6XkagW6Pq3HQm",
        "40 hours per week": "price_1LXNTxIeIfx6Xkag5iJ93qiM",
    },
}