import random
import string


def create_password():
    length = 12

    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    num = string.digits

    all = lower + upper + num

    temp = random.sample(all, length)

    password = "".join(temp)
    return password
