from rest_framework import serializers

from properties.models import Country, HardSkill, SoftSkill, FeedBack, FAQ, GroupHardSkill, Industry, Category


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = [
            'id',
            'name',
        ]


class HardSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = HardSkill
        fields = [
            'id',
            'name',
            'description',
            'is_checked',
        ]


class GroupHardSkillSerializer(serializers.ModelSerializer):
    hardskill_set = serializers.SerializerMethodField('get_package_count', read_only=True)

    def get_package_count(self, instance):
        hardskill_set = instance.hardskill_set.all().order_by('order')
        return HardSkillSerializer(hardskill_set, many=True).data

    class Meta:
        model = GroupHardSkill
        fields = [
            'id',
            'name',
            'avatar',
            'hardskill_set',
        ]


class SoftSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = SoftSkill
        fields = [
            'id',
            'name',
            'score_1',
            'score_2',
            'score_3',
            'score_4',
        ]


class FeedBackSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeedBack
        fields = [
            'id',
            'email',
            'name',
            'text',
        ]


class FAQSerializer(serializers.ModelSerializer):
    class Meta:
        model = FAQ
        fields = [
            'id',
            'question',
            'answer',
        ]


class IndustrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Industry
        fields = [
            'name',
        ]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = [
            'name',
        ]
