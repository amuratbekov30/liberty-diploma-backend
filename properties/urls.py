from django.urls import path
from properties.views import (
    countries_list, hard_skills_list, soft_skills_list, feedback_create, faq_list, get_search_properties
)

urlpatterns = [
    path('hardskills/', hard_skills_list, name='hard_skills_list'),
    path('countries/', countries_list, name='countries_list'),
    path('softskills/', soft_skills_list, name='soft_skills_list'),
    path('faq/', faq_list, name='faq_list'),
    path('feedback/', feedback_create, name='feedback_create'),
    path('search_props/', get_search_properties, name='get_search_properties'),
]
