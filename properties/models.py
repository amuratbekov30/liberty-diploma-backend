from django.db import models


class GroupHardSkill(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name='Название'
    )
    order = models.IntegerField(
        verbose_name='Порядковый номер',
        default=1
    )
    avatar = models.ImageField(
        verbose_name='Лого',
        upload_to="logo/",
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'Проф навык'
        verbose_name_plural = 'Проф навыки'

    def __str__(self):
        return self.name


class HardSkill(models.Model):
    group = models.ForeignKey(
        GroupHardSkill,
        verbose_name='skill',
        on_delete=models.SET_NULL,
        null=True,
    )
    name = models.CharField(
        max_length=50,
        verbose_name='Название'
    )
    order = models.IntegerField(
        verbose_name='Порядковый номер',
        default=1
    )
    is_checked = models.BooleanField(
        verbose_name='Это компания?',
        default=False,
    )
    description = models.CharField(
        max_length=500,
        verbose_name='Описание',
        blank=True,
        default=''
    )

    class Meta:
        verbose_name = 'Проф навык'
        verbose_name_plural = 'Проф навыки'

    def __str__(self):
        return self.name


class SoftSkill(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name='Название',
        default='',
        blank=True
    )
    order = models.IntegerField(
        verbose_name='Порядковый номер',
        default=1
    )
    score_1 = models.CharField(
        max_length=300,
        verbose_name='1 балл',
        default='',
        blank=True
    )
    score_2 = models.CharField(
        max_length=300,
        verbose_name='2 балл',
        default='',
        blank=True
    )
    score_3 = models.CharField(
        max_length=300,
        verbose_name='3 балл',
        default='',
        blank=True
    )
    score_4 = models.CharField(
        max_length=300,
        verbose_name='4 балл',
        default='',
        blank=True
    )

    class Meta:
        verbose_name = 'Гибкий навык'
        verbose_name_plural = 'Гибкие навыки'

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name='Название'
    )

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'

    def __str__(self):
        return self.name


class FeedBack(models.Model):
    email = models.CharField(
        max_length=120,
        verbose_name='Почта',
    )
    name = models.CharField(
        max_length=120,
        verbose_name='Имя',
    )
    text = models.CharField(
        max_length=500,
        verbose_name='Сообщение',
    )

    class Meta:
        verbose_name = 'Обратная связь'
        verbose_name_plural = 'Обратная связь'

    def __str__(self):
        return f'{self.id}'


class FAQ(models.Model):
    question = models.CharField(
        max_length=255,
        verbose_name='Название'
    )
    answer = models.TextField(
        verbose_name='Ответ'
    )

    class Meta:
        verbose_name = 'Частый вопрос'
        verbose_name_plural = 'Частые вопросы'

    def __str__(self):
        return f"{self.id}"


class Call(models.Model):
    email = models.CharField(
        max_length=255,
        verbose_name='Название'
    )
    name = models.CharField(
        max_length=255,
        verbose_name='Название'
    )
    phone = models.CharField(
        max_length=255,
        verbose_name='Название'
    )
    message = models.TextField(
        verbose_name='Ответ'
    )

    class Meta:
        verbose_name = 'Заявка с сайта'
        verbose_name_plural = 'Заявки с сайта'

    def __str__(self):
        return f"{self.email}"


class WorkTheme(models.Model):
    theme = models.CharField(
        max_length=255,
        verbose_name='Тема'
    )

    class Meta:
        verbose_name = 'Тема работы'
        verbose_name_plural = 'Темы работы'

    def __str__(self):
        return f"{self.theme}"


class Industry(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name='Индустрия'
    )

    class Meta:
        verbose_name = 'Индустрия например машины'
        verbose_name_plural = 'Индустрии'

    def __str__(self):
        return f"{self.name}"


class Category(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name='Тема'
    )

    class Meta:
        verbose_name = 'Категория например rich media'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return f"{self.name}"

