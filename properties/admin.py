from django.contrib import admin
from properties.models import HardSkill, SoftSkill, Country, FeedBack, FAQ, GroupHardSkill, Industry, Category


class HardSkillInline(admin.TabularInline):
    model = HardSkill
    extra = 0


class GroupHardSkillAdmin(admin.ModelAdmin):
    model = GroupHardSkill
    list_display = ('id', 'name', 'order',)
    search_fields = ('name', )
    inlines = [HardSkillInline]


class SoftSkillAdmin(admin.ModelAdmin):
    model = SoftSkill
    list_display = ('name', 'order')
    search_fields = ('name', )


class CountryAdmin(admin.ModelAdmin):
    model = Country
    list_display = ('name', )
    search_fields = ('name', )


class FeedBackAdmin(admin.ModelAdmin):
    model = FeedBack
    list_display = ('id', 'email', 'name', 'text')
    search_fields = ('id', )


class FAQAdmin(admin.ModelAdmin):
    model = FAQ
    list_display = ('id', 'question', 'answer')
    search_fields = ('question', )


class IndustryAdmin(admin.ModelAdmin):
    model = Industry
    list_display = ('id', 'name',)
    search_fields = ('name', )


class CategoryAdmin(admin.ModelAdmin):
    model = Category
    list_display = ('id', 'name',)
    search_fields = ('name', )


admin.site.register(FAQ, FAQAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Industry, IndustryAdmin)
admin.site.register(FeedBack, FeedBackAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(GroupHardSkill, GroupHardSkillAdmin)
admin.site.register(SoftSkill, SoftSkillAdmin)

