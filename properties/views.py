from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
import requests
from django.core.mail import send_mail
from django.conf import settings

from properties.models import HardSkill, SoftSkill, Country, FAQ, GroupHardSkill, Call, Industry, Category
from properties.serializers import (
    CountrySerializer, SoftSkillSerializer, HardSkillSerializer, FeedBackSerializer, FAQSerializer,
    GroupHardSkillSerializer, IndustrySerializer, CategorySerializer
)
from users.models import User


@api_view(['GET'])
@permission_classes((AllowAny,))
def countries_list(request):
    if request.method == 'GET':
        countries = Country.objects.all()
        serializer = CountrySerializer(countries, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def hard_skills_list(request):
    if request.method == 'GET':
        group_hard_skills = GroupHardSkill.objects.all().order_by('order')
        serializer = GroupHardSkillSerializer(group_hard_skills, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def soft_skills_list(request):
    if request.method == 'GET':
        soft_skills = SoftSkill.objects.all().order_by('order')
        serializer = SoftSkillSerializer(soft_skills, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def faq_list(request):
    if request.method == 'GET':
        faqs = FAQ.objects.all()
        serializer = FAQSerializer(faqs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def feedback_create(request):
    if request.method == 'POST':
        serializer = FeedBackSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {"message": f"Your application number №{serializer.data['id']}. Wait for the manager's response"},
                status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_search_properties(request):
    if request.method == 'GET':
        names = []
        category_list = []
        industry_list = []
        users = User.objects.filter(position__isnull=False)
        for user in users:
            names.append(f'{user.first_name} {user.last_name}')
        industries = Industry.objects.all()
        for industry in industries:
            industry_list.append(f'{industry.name}')
        categories = Category.objects.all()
        for category in categories:
            category_list.append(f'{category.name}')
        return Response({
            "designers": names,
            "categories": category_list,
            "industries": industry_list
         },
            status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_designers_list(request):
    if request.method == 'GET':
        names = []
        users = User.objects.filter(position__isnull=False)
        for user in users:
            names.append(f'{user.first_name} {user.last_name}')
        return Response(names, status=status.HTTP_200_OK)
