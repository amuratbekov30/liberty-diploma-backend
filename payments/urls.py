from django.urls import path
from payments.views import (
    customer_portal, create_checkout_session, invoices_list
)

urlpatterns = [
    # path('webhook/', webhook_received, name='webhook_received'),
    path('create-checkout-session/', create_checkout_session, name='create_checkout_session'),
    path('create-portal-session/', customer_portal, name='customer_portal'),
    path('invoices/', invoices_list, name='invoices_list'),
    # path('create-payment-intent/', create_payment, name='create_payment'),
]
