from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
import stripe
from common.variables import DOMAIN, PRICES

stripe.api_key = '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'


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def create_checkout_session(request):
    if request.method == 'POST':
        try:
            level = request.data['level']
            work_time = request.data['work_time']
            # prices = stripe.Price.list(
            #     product=request.data['lookup_key'],
            #     expand=['data.product']
            # )
            # print(prices)
            # print(prices.data)
            checkout_session = stripe.checkout.Session.create(
                line_items=[
                    {
                        'price': PRICES[level][work_time],
                        'quantity': 1,
                    },
                ],
                customer=request.user.customer_id,
                mode='subscription',
                success_url=DOMAIN +
                            '?success=true&session_id={CHECKOUT_SESSION_ID}',
                cancel_url=DOMAIN + '?canceled=true',
            )
            return Response(checkout_session.url, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response({'message': "Server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def customer_portal(request):
    if request.method == 'POST':
        # For demonstration purposes, we're using the Checkout session to retrieve the customer ID.
        # Typically this is stored alongside the authenticated user in your database.
        checkout_session_id = request.data['sessionId']
        checkout_session = stripe.checkout.Session.retrieve(checkout_session_id)

        # This is the URL to which the customer will be redirected after they are
        # done managing their billing with the portal.
        return_url = DOMAIN

        portalSession = stripe.billing_portal.Session.create(
            customer=checkout_session.customer,
            return_url=return_url,
        )
        return Response(portalSession.url, status=status.HTTP_200_OK)

# @api_view(['POST'])
# @permission_classes((AllowAny,))
# def webhook_received(request):
#     if request.method == 'POST':
#         # Replace this endpoint secret with your endpoint's unique secret
#         # If you are testing with the CLI, find the secret by running 'stripe listen'
#         # If you are using an endpoint defined with the API or dashboard, look in your webhook settings
#         # at https://dashboard.stripe.com/webhooks
#         webhook_secret = 'whsec_12345'
#         request_data = json.loads(request.data)
#
#         if webhook_secret:
#             # Retrieve the event by verifying the signature using the raw body and secret if webhook signing is configured.
#             signature = request.headers.get('stripe-signature')
#             try:
#                 event = stripe.Webhook.construct_event(
#                     payload=request.data, sig_header=signature, secret=webhook_secret)
#                 data = event['data']
#             except Exception as e:
#                 return e
#             # Get the type of webhook event sent - used to check the status of PaymentIntents.
#             event_type = event['type']
#         else:
#             data = request_data['data']
#             event_type = request_data['type']
#         data_object = data['object']
#
#         print('event ' + event_type)
#
#         if event_type == 'checkout.session.completed':
#             print('🔔 Payment succeeded!')
#         elif event_type == 'customer.subscription.trial_will_end':
#             print('Subscription trial will end')
#         elif event_type == 'customer.subscription.created':
#             print('Subscription created %s', event.id)
#         elif event_type == 'customer.subscription.updated':
#             print('Subscription created %s', event.id)
#         elif event_type == 'customer.subscription.deleted':
#             # handle subscription canceled automatically based
#             # upon your subscription settings. Or if the user cancels it.
#             print('Subscription canceled: %s', event.id)
#
#         return jsonify({'status': 'success'})


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def invoices_list(request):
    if request.method == 'GET':
        try:
            # starting_after = request.query_params.get('starting_after')
            if request.user.customer_id:
                customer_id = request.user.customer_id
            else:
                user = request.user
                customer = stripe.Customer.create(email=user.username)
                user.customer_id = customer.id
                user.save()
                customer_id = customer.id
            # # Cancel the subscription by deleting it
            # subscriptions = stripe.Subscription.list(
            #     customer=customer_id,
            #     status='all',
            #     expand=['data.default_payment_method']
            # )
            # for subscription in subscriptions:
            #     # Retrieve the subscription
            #     subscription = stripe.Subscription.retrieve(subscription_id)
            invoices = stripe.Invoice.list(
                limit=100, customer=customer_id,
                expand=['data.payment_intent']
            )
            return Response(invoices.data, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response({'message': "Server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

#
# @api_view(['POST'])
# @permission_classes((IsAuthenticated,))
# def create_payment(request):
#     if request.method == 'GET':
#         try:
#             data = request.data
#             # Create a PaymentIntent with the order amount and currency
#             # Retrive the Invoice
#             # invoice = stripe.Invoice.upcoming(
#             #     customer=request.user.customer_id,
#             #     subscription=request.data['subscription_id'],
#             # )
#             # intent = stripe.PaymentIntent.create(
#             #     amount=calculate_order_amount(data['items']),
#             #     currency='usd',
#             #     automatic_payment_methods={
#             #         'enabled': True,
#             #     },
#             # )
#
#             # return Response({'clientSecret': intent['client_secret']}, status=status.HTTP_200_OK)
#         except Exception as e:
#             print(e)
#             return Response({'message': "Server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
