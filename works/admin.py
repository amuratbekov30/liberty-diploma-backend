from django.contrib import admin

from works.models import Work, Attachment


class AttachmentInline(admin.TabularInline):
    model = Attachment
    extra = 0


class WorkAdmin(admin.ModelAdmin):
    model = Work
    list_display = ('name', 'designer', 'cost', 'resize_cost', 'description')
    search_fields = ('name', )
    inlines = [AttachmentInline]


admin.site.register(Work, WorkAdmin)
# Register your models here.
