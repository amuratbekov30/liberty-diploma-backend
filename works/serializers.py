from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from properties.serializers import IndustrySerializer, CategorySerializer
from users.serializers import ShortDesignerSerializer, CompanySerializer
from works.models import Work, Attachment


class AttachmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Attachment
        fields = '__all__'


class WorkSerializer(serializers.ModelSerializer):
    designer = ShortDesignerSerializer(read_only=True)
    customer = CompanySerializer(read_only=True)
    industry = IndustrySerializer(read_only=True)
    category = CategorySerializer(read_only=True)
    # attachment = AttachmentSerializer(read_only=True)
    attachment_set = SerializerMethodField('get_attachs', read_only=True)

    def get_attachs(self, instance):
        attach_set = Attachment.objects.filter(work=instance)
        return AttachmentSerializer(attach_set, many=True).data

    class Meta:
        model = Work
        fields = '__all__'
