from django.db import models

from properties.models import Industry, Category
from users.models import User


class Work(models.Model):
    STATUS_OPTIONS = [
        ('Portfolio', 'Portfolio'),
        ('Declined', 'Declined'),
        ('Accepted', 'Accepted'),
        ('Waiting', 'Waiting'),
        ('To Do', 'To Do'),
    ]
    name = models.CharField(
        max_length=255,
        verbose_name='Название'
    )
    comment = models.CharField(
        max_length=255,
        verbose_name='Комментарий',
        null=True,
        blank=True
    )
    designer = models.ForeignKey(
        User,
        verbose_name='Автор',
        related_name='designer',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    skills = models.CharField(
        max_length=500,
        verbose_name='Требуемые скилы',
        null=True,
        blank=True
    )
    customer = models.ForeignKey(
        User,
        verbose_name='Заказчик',
        related_name='customer',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    industry = models.ForeignKey(
        Industry,
        verbose_name='Industry',
        on_delete=models.CASCADE,
        null=True
    )
    status = models.CharField(
        max_length=255,
        verbose_name='Статус',
        choices=STATUS_OPTIONS
    )
    category = models.ForeignKey(
        Category,
        verbose_name='Category',
        on_delete=models.CASCADE,
        null=True
    )
    cost = models.IntegerField(
        default=0,
        verbose_name='Цена разработки',
        null=True,
        blank=True
    )
    resize_cost = models.IntegerField(
        default=0,
        verbose_name='Дополнительная цена',
        null=True,
        blank=True
    )
    description = models.TextField(
        max_length=500,
        verbose_name='Дополнительная цена'
    )
    file = models.FileField(
        verbose_name='файл',
        upload_to='content/',
        null=True,
        blank=True
    )
    avatar = models.ImageField(
        verbose_name='Иконка',
        upload_to='avatars/',
        null=True,
        blank=True
    )
    rich_link = models.CharField(
        max_length=500,
        verbose_name='Путь к ричке',
        null=True,
        blank=True
    )
    deadline = models.DateTimeField(
        verbose_name='Дата создания',
        null=True,
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )

    class Meta:
        verbose_name = 'Работа'
        verbose_name_plural = 'Работы'

    def __str__(self):
        return f"{self.name}"


class Attachment(models.Model):
    work = models.ForeignKey(
        Work,
        verbose_name='Работа',
        on_delete=models.CASCADE
    )
    file = models.FileField(
        verbose_name='файл',
        upload_to='content/',
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'Вложение'
        verbose_name_plural = 'Вложения'

    def __str__(self):
        return f"{self.file}"

# Create your models here.
