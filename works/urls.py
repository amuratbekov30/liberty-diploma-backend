from django.urls import path

from works.views import get_all_works, get_work, create_work, get_properties, all_requests_designer, \
    my_requests_designer, my_requests_customer, new_requests

urlpatterns = [
    # new paths
    path('all/', get_all_works, name='get_all_works'),
    path('work/<int:work_id>/', get_work, name='get_work'),
    path('create/', create_work, name='create_work'),
    path('get_properties/', get_properties, name='get_properties'),
    path('all_requests_designer/', all_requests_designer, name='all_requests_designer'),
    path('new_requests/', new_requests, name='new_requests'),
    path('my_requests_designer/', my_requests_designer, name='my_requests_designer'),
    path('my_requests_customer/', my_requests_customer, name='my_requests_customer'),
]
