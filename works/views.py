from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from datetime import datetime
from properties.models import Industry, Category
from properties.serializers import IndustrySerializer, CategorySerializer
from works.models import Work, Attachment
from works.serializers import WorkSerializer


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_all_works(request):
    works = Work.objects.filter(status='Portfolio')
    serializer = WorkSerializer(works, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_work(request, work_id):
    work = Work.objects.filter(id=work_id).first()
    serializer = WorkSerializer(work)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_properties(request):
    industry = Industry.objects.all()
    categories = Category.objects.all()
    serializer = IndustrySerializer(industry, many=True)
    serializer_p = CategorySerializer(categories, many=True)
    return Response({"industry": serializer.data, "category": serializer_p.data}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def create_work(request):
    name = request.data['title']
    customer = request.user
    industry = request.data['industry']
    category = request.data['category']
    cost = request.data['cost']
    resize_cost = request.data['resize_cost']
    description = request.data['description']
    deadline = request.data['deadline']
    skills = request.data['skills']
    p_industry = Industry.objects.filter(name=industry).first()
    p_category = Category.objects.filter(name=category).first()
    if customer.is_staff:
        work = Work(
            name=name,
            designer=customer,
            status="Portfolio",
            industry=p_industry,
            category=p_category,
            cost=cost,
            resize_cost=resize_cost,
            description=description,
            skills=skills
        )
        work.save()
        return Response({"work_id": work.id}, status=status.HTTP_200_OK)
    else:
        work = Work(
            name=name,
            customer=customer,
            status="To Do",
            industry=p_industry,
            category=p_category,
            cost=cost,
            resize_cost=resize_cost,
            description=description,
            deadline=datetime.strptime(deadline, '%d.%m.%Y'),
            skills=skills
        )
        work.save()
        return Response({"work_id": work.id}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def new_requests(request):
    request = Work.objects.filter(status='To Do')
    serializer = WorkSerializer(request, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def all_requests_designer(request):
    request = Work.objects.filter(designer__isnull=True)
    serializer = WorkSerializer(request, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def my_requests_designer(request):
    request = Work.objects.filter(designer=request.user)
    serializer = WorkSerializer(request, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def my_requests_customer(request):
    request = Work.objects.filter(customer=request.user)
    serializer = WorkSerializer(request, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


# Create your views here.
